<!DOCTYPE html>
<html lang="en">
<head>
    <title>Chessboard with PHP, HTML and CSS </title>
    <style type="text/css">
        div {
            float: left;
            width: 50px;
            height: 50px;
            border:5px solid;
        }

        .black_square {
            background: black;
        }
        .white_square{
            background: white;
        }

    </style>

</head>
<body>
<?php

echo "<form action='' method='post'>
       <label for='number'>Enter Number To Give input</label>
       <input type='text' name='number'>
       <input type='submit' >
       </form>";


        $number = $_POST['number'];

for ($i=0;$i<$number;$i++) {

    for($j=0;$j<$number;$j++){
        if($j%$number==0) echo "<br style=\"clear:both\" />";
        if($j%2==0){
            if($i%2==0)
            {
                echo "<div class='white_square'></div>";
            }
            else{
                echo "<div class='black_square'></div>";
            }
        }
        else{
            if($i%2!=0)
            {
                echo "<div class='white_square'></div>";
            }
            else{
                echo "<div class='black_square'></div>";
            }
        }

    }

}

?>
</body>
</html>
